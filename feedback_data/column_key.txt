feedback.csv
status              status displayed on website of package when feedback is left
dbio_lastchanged    date entry was modified (or created)
status_code         status code from carrier
delivery_date_tz    time zone for delivery
unique_id           unique identifier
itemcond_score      "Rate the item condition when delivered."
overall_score       "rate the overall delivery and tracking experience"
received_on_tz      time zone when the package was received on
received_on         when the package was received
uidesign_score      "rate the UI. Is it easy to use?"
ship_date_tz        timezone for ship date
score_value         "rate the tracking experience"
dbio_rowid          row_id in database
eta_date_tz         time zone for eta
dbio_created_on     when the entry was created
long_comment        long form comment feedback 
ship_date           when package was shipped
packaging_score     "Rate the packaging"
dbio_created_by     where the entry came from
eta_date            estimated time of arrival
dbio_verid          number of times entry has been updated
short_comment       customers report of the status of the item
ccare_required      whether the customer has clicked the customer care button
delivery_date       date package was delivered


sentiment_graded.csv
sentiment           0=not important, 1=important(negative feedback)
long_comment        text of comment
